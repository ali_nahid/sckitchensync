﻿using KitchenSync.Lib.Models.Interfaces.Core;
using KitchenSync.Lib.Models.Objects.Navigation;
using KitchenSync.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KitchenSync.Navigation.MVC.Controllers
{
    public class NavigationController : Controller
    {
        private IGeneralService generalService;
        /*public NavigationController(IGeneralService generalService)
        {
            this.generalService = generalService;
        }*/

        public ActionResult Index()
        {
            KitchenSync.Services.GeneralService gs = new Services.GeneralService();
            List<NavigationItem> items= gs.GetNavigationItems();
            return View("/Views/KitchenSync/Navigation/Menu.cshtml", items);
        }
    }
}