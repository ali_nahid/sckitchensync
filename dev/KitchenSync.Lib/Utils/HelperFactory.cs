﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSync.Lib.Utils
{
    public class HelperFactory
    {
        public static string PTaggedHTML(string txt)
        {
            return txt != null && txt.Contains("<p>") ? txt : "<p>" + txt + "</p>";
        }
    }
}
