﻿using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using KitchenSync.Lib.Models.Interfaces;
using KitchenSync.Lib.Models.Interfaces.Core;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Converters;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace KitchenSync.Lib.Models.Objects.Core
{
    public class SCItem : ISCItem
    {
        [SitecoreId]
        [IndexField("_group")]
        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        public ID Id { get; set; }

        [SitecoreInfo(SitecoreInfoType.Language)]
        [IndexField("_language")]
        public string Language { get; set; }

        [SitecoreInfo(SitecoreInfoType.Version)]
        public int Version { get { return 0; } }

        [TypeConverter(typeof(IndexFieldItemUriValueConverter))]
        [XmlIgnore]
        [IndexField("_uniqueid")]
        public ItemUri Uri { get; set; }

        public string DisplayName { get; set; }

        public string Name { get; set; }

        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        [IndexField("_template")]
        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        public ID TemplateId { get; set; }

        [SitecoreInfo(SitecoreInfoType.Url, UrlOptions = SitecoreInfoUrlOptions.LanguageEmbeddingNever)]
        [IndexField("urllink")]
        public string Url { get; set; }


        [SitecoreField(FieldName = "__Updated")]
        public DateTime Updated { get; set; }

        [SitecoreField(FieldName = "__Created")]
        public DateTime Created { get; set; }

        [SitecoreChildren(InferType = true, IsLazy = true)]
        public IEnumerable<ISCItem> Children { get; set; }

        //[IndexField("_parent")]
        public ISCItem Parent { get; set; }

        [SitecoreInfo(SitecoreInfoType.Path)]
        [IndexField("_fullpath")]
        public string Path { get; set; }
    }
}
