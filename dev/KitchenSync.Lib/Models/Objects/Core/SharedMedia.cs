﻿using Glass.Mapper.Sc.Configuration.Attributes;
using KitchenSync.Lib.Models.Interfaces.Core;


namespace KitchenSync.Lib.Models.Objects.Core
{
    [SitecoreType(TemplateId = "{6636EAF7-49C4-4B24-8700-1CE85DAA0E33}")]
    public class SharedMedia
    {
        [SitecoreField(FieldId = "{AE1706C1-BABD-44E8-85AF-725471A68AB4}")]
        public Glass.Mapper.Sc.Fields.Image BannerImage { get; set; }
        //[SitecoreField(FieldId = "{9C4E54BD-9433-49A0-AAEB-3078E91CDA1B}")]
        //public Glass.Mapper.Sc.Fields.Image BackgroundImage { get; set; }
        //[SitecoreField(FieldId = "{F098E9F6-E85D-4EEC-8C56-F94639B958BD}")]
        //public Glass.Mapper.Sc.Fields.Image ThumbnailImage { get; set; }
    }
}
