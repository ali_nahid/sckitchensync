﻿using Glass.Mapper.Sc.Configuration.Attributes;
using KitchenSync.Lib.Models.Interfaces.Core;

namespace KitchenSync.Lib.Models.Objects.Core
{
    public class GeneralContent : SCItem, IGeneralContent
    {   
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
