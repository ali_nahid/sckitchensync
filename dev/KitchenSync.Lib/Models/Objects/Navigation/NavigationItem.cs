﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSync.Lib.Models.Objects.Navigation
{
    public class NavigationItem
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }
}
