﻿using Glass.Mapper.Sc.Configuration.Attributes;
using KitchenSync.Lib.Models.Interfaces.Core;

namespace KitchenSync.Lib.Models.Interfaces.Video
{
    [SitecoreType(TemplateId = "{C20B6EFD-0E94-48EA-A735-0EBEA3408C18}")]
    public interface IVideoContent : IGeneralContent
    {
        [SitecoreField(FieldName = "YouTubeHTML")]
        string YouTubeHTML { get; set; }
    }
}
