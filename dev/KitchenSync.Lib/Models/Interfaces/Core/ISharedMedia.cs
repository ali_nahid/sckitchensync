﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace KitchenSync.Lib.Models.Interfaces.Core
{
    [SitecoreType(TemplateId = "{6636EAF7-49C4-4B24-8700-1CE85DAA0E33}")]
    public interface ISharedMedia
    {
        [SitecoreField(FieldId = "{AE1706C1-BABD-44E8-85AF-725471A68AB4}")]
        Glass.Mapper.Sc.Fields.Image BannerImage { get; set; }
        [SitecoreField(FieldId = "{9C4E54BD-9433-49A0-AAEB-3078E91CDA1B}")]
        Glass.Mapper.Sc.Fields.Image BackgroundImage { get; set; }
        [SitecoreField(FieldId = "{F098E9F6-E85D-4EEC-8C56-F94639B958BD}")]
        Glass.Mapper.Sc.Fields.Image ThumbnailImage { get; set; }
    }
}
