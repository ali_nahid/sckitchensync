﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data;
using Sitecore.Globalization;
using Glass.Mapper.Sc.Configuration;
using Sitecore.ContentSearch;
using System.ComponentModel;
using Sitecore.ContentSearch.Converters;
using System.Xml.Serialization;

namespace KitchenSync.Lib.Models.Interfaces.Core
{
    [SitecoreType(AutoMap=true)]
    public interface ISCItem
    {
        [SitecoreId]
        [IndexField("_group")]
        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        ID Id { get; set; }

        [SitecoreInfo(SitecoreInfoType.Language)]
        [IndexField("_language")]
        string Language { get; set; }

        [SitecoreInfo(SitecoreInfoType.Version)]
        int Version { get; }

        [TypeConverter(typeof(IndexFieldItemUriValueConverter))]
        [XmlIgnore]
        [IndexField("_uniqueid")]
        ItemUri Uri { get; set; }

        [SitecoreInfo(SitecoreInfoType.DisplayName)]
        string DisplayName { get; set; }

        [SitecoreInfo(SitecoreInfoType.Name)]
        string Name { get; set; }

        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        [IndexField("_template")]
        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        ID TemplateId { get; set; }

        [SitecoreInfo(SitecoreInfoType.Url, UrlOptions = SitecoreInfoUrlOptions.LanguageEmbeddingNever)]
        [IndexField("urllink")]
        string Url { get; set; }


        [SitecoreField(FieldName = "__Updated")]
        DateTime Updated { get; set; }

        [SitecoreField(FieldName = "__Created")]
        DateTime Created { get; set; }

        [SitecoreChildren(InferType = true, IsLazy=true)]
        IEnumerable<ISCItem> Children { get; set; }

        //[IndexField("_parent")]
        ISCItem Parent { get; set; }

        [SitecoreInfo(SitecoreInfoType.Path)]
        [IndexField("_fullpath")]
        string Path { get; set; }

        //[IndexField("_path")]
        //[TypeConverter(typeof(IndexFieldEnumerableConverter))]
        //IEnumerable<ID> Paths { get; set; }


        // Will be set with key and value for each field in the index document
        //string this[string key] { get; set; }

    }
}
