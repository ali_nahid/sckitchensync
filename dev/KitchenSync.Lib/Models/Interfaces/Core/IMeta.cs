﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace KitchenSync.Lib.Models.Interfaces.Core
{
    [SitecoreType(TemplateId = "{DCE700E6-C24B-4672-B7FE-7CED6C448D12}")]
    public interface IMeta
    {
        [SitecoreField(FieldId = "{17C782D0-07C5-4701-A4D3-EEAA674EA123}")]
        string MetaTitle { get; set; }
        [SitecoreField(FieldId = "{961331C8-BC90-49E0-AB4C-ADDF3079D6E7}")]
        string MetaKeyword { get; set; }
        [SitecoreField(FieldId = "{053CE974-43E6-46EC-A44D-8103FF191EA0}")]
        string MetaDescription { get; set; }
    }
}
