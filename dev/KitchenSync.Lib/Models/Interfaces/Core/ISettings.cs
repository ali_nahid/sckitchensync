﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace KitchenSync.Lib.Models.Interfaces.Core
{
    [SitecoreType(TemplateId = "{443EB168-8E99-4CD1-A07F-53A5C449AFF5}")]
    public interface ISettings : ISCItem
    {
        [SitecoreField(FieldId = "{732DEBF5-D6EC-4BDE-9631-A09052622349}")]
        bool IsShowInNavigation { get; set; }
    }
}
