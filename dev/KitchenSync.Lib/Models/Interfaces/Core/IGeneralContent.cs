﻿
using Glass.Mapper.Sc.Configuration.Attributes;
namespace KitchenSync.Lib.Models.Interfaces.Core
{
    [SitecoreType(TemplateId = "{B7A7B7F8-4239-4B8A-8980-97BAED7D2239}")]
    public interface IGeneralContent : ISCItem
    {
        [SitecoreField(FieldName="Title")]
        string Title { get; set; }
        [SitecoreField(FieldName = "Description")]
        string Description { get; set; }
    }
}
