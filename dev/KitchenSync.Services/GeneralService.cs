﻿using Glass.Mapper.Sc;
using KitchenSync.Lib.Models.Interfaces.Core;
using KitchenSync.Lib.Models.Objects.Navigation;
using KitchenSync.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSync.Services
{
    public class GeneralService : IGeneralService
    {
        public T GetItem<T>(Guid itemId) where T : class
        {
            Sitecore.Data.Items.Item item = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(itemId));
            return item.GlassCast<T>(isLazy:true);
        }

        public List<NavigationItem> GetNavigationItems()
        {
            List<NavigationItem> navList = new List<NavigationItem>();
            ISettings home = this.GetItem<ISettings>(new Guid(KitchenSync.Lib.Resources.Items.KitchenSyncHome));
            if (home.IsShowInNavigation)
            {
                navList.Add(new NavigationItem { Name = home.DisplayName, URL = home.Url });
            }
            SitecoreContext context = new SitecoreContext(Sitecore.Context.Database);
            foreach (ISCItem item in home.Children)
            {
                ISettings sItem = context.GetItem<ISettings>(item.Id.Guid);
                if (sItem.IsShowInNavigation)
                {
                    navList.Add(new NavigationItem { Name = sItem.DisplayName, URL = sItem.Url });
                }
            }
            return navList;
        }
    }
}
