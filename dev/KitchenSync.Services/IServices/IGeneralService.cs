﻿using KitchenSync.Lib.Models.Objects.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenSync.Services.IServices
{
    public interface IGeneralService
    {
        T GetItem<T>(Guid itemId) where T : class;
        List<NavigationItem> GetNavigationItems();
    }
}
