param($projectName = "KitchenSync", 
$destinationDir = "web\website")

$realProjectName = $projectName 

$destinationDir = "$destinationDir\Views\$projectName"
$projectName = "dev\$projectName.MVC"

$bool = Test-Path($destinationDir)
if($bool -eq $false)
{
	New-Item -path $destinationDir -type directory
}

$srcProjects = Get-ChildItem $projectName | Where { $_.PSIsContainer } | Select-Object FullName


#clear destination folder
foreach($srcProject in $srcProjects)
{
	$x = $srcProject.FullName
	$srcViewDir = Get-ChildItem "$x\Views\$realProjectName" | Where { $_.PSIsContainer } | Select-Object Name
	foreach($src in $srcViewDir)
	{	
		$xx = $src.Name
		Write-Host "OKAY $xx" -foregroundcolor Green -backgroundcolor black
		$bool = Test-Path("$destinationDir\$xx")
		if($bool -eq $true)
		{
			Write-Output "deleting from destination $destinationDir\$xx"
			Remove-Item -Recurse -Force "$destinationDir\$xx"
			#remove
		}
	}
}


#copy
foreach($srcProject in $srcProjects)
{
	$x = $srcProject.FullName
	$srcViewDir = Get-ChildItem "$x\Views\$realProjectName" | Where { $_.PSIsContainer } | Select-Object Name
	foreach($src in $srcViewDir)
	{	
		$xx = $src.Name
		Write-Output "copy $x\Views\$realProjectName\$xx to $destinationDir"
		Copy-Item "$x\Views\$realProjectName\$xx" $destinationDir -recurse -force
		#copy recurse
	}
	
}

